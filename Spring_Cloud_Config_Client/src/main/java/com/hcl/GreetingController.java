package com.hcl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class GreetingController {

	@Autowired
	ChannelInfo info;
 
    @GetMapping("greeting")
    public String getGreeting() {
    	String greeting=info.getUrl();
        return greeting;
    }
}
