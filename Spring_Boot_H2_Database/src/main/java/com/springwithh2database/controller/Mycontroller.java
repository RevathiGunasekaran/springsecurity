package com.springwithh2database.controller;

import java.awt.PageAttributes.MediaType;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springwithh2database.model.User;
import com.springwithh2database.service.UserService;

@RestController
public class Mycontroller {
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/hello")
	public String sayHello() {
		return "welcome to bank project";
	}

	@GetMapping(value = "/read")
	public List<User> readAll() {
		
		return userService.readAll();
	}

	@PostMapping(value = "/create",consumes=org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public User createUser(@RequestBody User user) {
	    System.out.println(user.getPassword());
		return userService.createUser(user);

	}

	@PutMapping(value = "/update")
	public User updateUser(@Valid @RequestBody User user) {
		return userService.updateUser(user);
	}

	@DeleteMapping(value = "/delete/{alias1}")
	public void deleteUser(@PathVariable("alias1") int arg) {
		userService.deleteUser(arg);
	}
	


}
