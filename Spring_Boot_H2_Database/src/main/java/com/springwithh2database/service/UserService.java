package com.springwithh2database.service;

import java.util.List;

import com.springwithh2database.model.User;

public interface UserService {
	User createUser(User user);

	List<User> readAll();

	User readUserByName(String UserName);

	User updateUser(User user);

	public void deleteUser(int Userid);


}
