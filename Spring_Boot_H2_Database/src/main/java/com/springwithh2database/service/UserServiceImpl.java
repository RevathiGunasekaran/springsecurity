package com.springwithh2database.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springwithh2database.dao.UserRepositroy;
import com.springwithh2database.model.User;


@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepositroy userRepositoy;

	@Override
	public User createUser(User user) {
		User result = this.userRepositoy.save(user);
		return result;

	}

	
	public List<User> readAll() {
	
		List<User> listAll=(List<User>)userRepositoy.findAll();
		return listAll;
	}

	@Override
	public User readUserByName(String UserName) {
		
		return null;
	}

	@Override
	public User updateUser(User user) {
		return this.userRepositoy.save(user);
	}

	@Override
	public void deleteUser(int Userid) {
		Optional<User> option = this.userRepositoy.findById(Userid);
		User user = null;
		if (option.isPresent()) {
			user = option.get();
			userRepositoy.delete(user);
		}

	}
}
