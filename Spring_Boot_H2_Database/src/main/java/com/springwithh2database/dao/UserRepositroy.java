package com.springwithh2database.dao;

import org.springframework.data.repository.CrudRepository;

import com.springwithh2database.model.User;

public interface UserRepositroy extends CrudRepository<User,Integer>{

}
