package com.springwithh2database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
	
public class Springwithh2databaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springwithh2databaseApplication.class, args);

	}
}
