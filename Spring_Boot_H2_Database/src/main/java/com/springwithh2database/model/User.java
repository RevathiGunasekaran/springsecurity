package com.springwithh2database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tbl_user")
public class User implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	@Column(nullable = false)
	@Size(min=3,max=9,message="the username is sixe min to max")
	@NotEmpty(message="the user name is not empty")
	private String userName;
	@Column(nullable = false)
	private String password;
	private String profile;

	public User() {
		super();
	}

	public User(String userName, String password, String profile) {
		super();
		this.userName = userName;
		this.password = password;
		this.profile = profile;
	}

	public User(int userId, String userName, String password, String profile) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.profile = profile;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

}
