package com.javainuse.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerControllerClient {
	 @Autowired
	    RestTemplate restTemplate;

	    @RequestMapping(value = "/employees", method = RequestMethod.GET)
	  
	    public String getEmployee() {
	        String result = restTemplate.exchange("http://employee-producer/employee",HttpMethod.GET, null, String.class).getBody();
	        return result;
	    }
}