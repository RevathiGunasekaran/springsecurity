package com.javainuse;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
//https://www.logicbig.com/tutorials/spring-framework/spring-cloud/rest-template-load-balancer.html
//https://www.appsdeveloperblog.com/loadbalanced-resttemplate-call-internal-microservice/
//https://www.logicbig.com/tutorials/spring-framework/spring-cloud/load-balancing-and-multiple-microservice-instances.html
@SpringBootApplication
@EnableDiscoveryClient
public class SpringBootHelloWorldApplication {

	public static void main(String[] args) throws RestClientException, IOException {
		SpringApplication.run(SpringBootHelloWorldApplication.class, args);
	}

	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
