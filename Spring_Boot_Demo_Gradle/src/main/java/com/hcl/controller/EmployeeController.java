package com.hcl.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.hcl.model.Employee;
import com.hcl.service.EmployeeServiceIntf;

@RestController
public class EmployeeController {
	@Autowired
	EmployeeServiceIntf service;
	@GetMapping(value = "/welcome")
	public String empform() {
		return "Hello welcome to hcl";
	}
	@PostMapping(value="/saveEmployee")
	public String saveEmployee(@RequestBody Employee emp)
	{
		service.saveData(emp);
		return "saved successfully";
	}
	@GetMapping(value = "/fetchEmployees")
	public List<Employee> fetchEmployees() {
		List<Employee> ls=service.fetchEmployees();
		return ls;
	}
	@GetMapping(value = "/fetchByEmplName/{name}")
	public List<Employee> fetchByEmplName(@PathVariable("name") String name) {
		List<Employee> ls=service.fetchByEmplName(name);
	
		return ls;
	}
	@DeleteMapping(value = "/deleteEmployee/{id}")
	public ResponseEntity<String> deleteEmployee(@PathVariable("id") int id)throws Exception {
		Optional<Employee> emp=service.findById(id);
		if(emp.isPresent())
		{
		service.deleteEmployee(id);
		return new ResponseEntity("deleted successfully",HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity("Emp id not found",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
}
