package com.hcl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.SpringServletContainerInitializer;

@SpringBootApplication
public class SpringBootApp1{
    public static void main(String[] args) {
		SpringApplication.run(SpringBootApp1.class, args);
	}
}

/*
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class SpringBootApp1 {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootApp1.class, args);
	}
	
}
*/