package com.hcl.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.model.Employee;

@Repository
public interface EmployeeRespository extends JpaRepository<Employee,Integer>{

	@Query("select e from Employee e where e.name=lower(:name)")
	List<Employee> findByName(@Param("name") String name);//select * from table where name=?

	

}
