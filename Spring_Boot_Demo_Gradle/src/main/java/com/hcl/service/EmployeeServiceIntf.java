package com.hcl.service;

import java.util.List;
import java.util.Optional;

import com.hcl.model.Employee;

public interface EmployeeServiceIntf {

	void saveData(Employee emp);

	List<Employee> fetchEmployees();

	List<Employee> fetchByEmplName(String name);

	void deleteEmployee(int id);

	Optional<Employee> findById(int id);

}
