package com.hcl.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.model.Employee;
import com.hcl.repo.EmployeeRespository;

@Service
@Transactional
public class EmployeeServiceImpl  implements EmployeeServiceIntf{

	@Autowired
	EmployeeRespository repo;
	public void saveData(Employee emp) {
	
		repo.save(emp);
	}
	
	public List<Employee> fetchEmployees() {
	List<Employee> ls=repo.findAll();
		return ls;
	}

	
	public List<Employee> fetchByEmplName(String name) {
	List<Employee> ls=repo.findByName(name);
		return ls;
		
	}

	
	public void deleteEmployee(int id) {
		repo.deleteById(id);		
	}
  
	
	public Optional<Employee> findById(int id) {
	
		return repo.findById(id);
	}

}
