package com.test;

public final class Employee {
	private final String empName;
	private  AnotherAddress empAddress;
	public Employee(String empName, AnotherAddress empAddress)
	{
		this.empName=empName;
		this.empAddress=empAddress;
	}
	public String getEmpName() {
		return empName;
	}
	
	public AnotherAddress getEmpAddress() {
		
		return empAddress;
	
		
	}
	
	

}
