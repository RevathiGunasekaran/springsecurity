package com.test;

public class Address implements Cloneable {
	private String city;

	private String streetName;

	public Address(String city, String streetName) {
		this.city = city;
		this.streetName = streetName;
	}
	public Address()
	{
		
	}

	public String getCity() {
		return city;
	}

	public String getStreetName() {
		return streetName;
	}
	

	

}
