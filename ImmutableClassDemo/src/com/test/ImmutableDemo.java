package com.test;

public class ImmutableDemo {
public static void main(String args[]) throws CloneNotSupportedException
{
	Employee emp=new Employee("revathi",new AnotherAddress("Chennai","VGN Nagar"));
	AnotherAddress address=emp.getEmpAddress();
	address.setCity("Bangalore");
	System.out.println(address.getCity()+" "+address.getStreetName());
	
}
}
