package com.hcl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.SpringServletContainerInitializer;
/*Download and start Elastic Search,Logstash,Kibana
 * Inside Kibana set the path of elasticsearch
 * create spring boot application
 * create log file
 * copy the logstash.conf inside the logstash
 * start logstash
 * start the appliaction and hit the URL
 * we can see the log details in logstash
 * http://localhost:9200/_cat/indices -to get the indexes of logstash
 * get the index value
 * open kibana-->management->index pattern-->create index pattern-->i dont want timestamp-->create index pattern
 * click on discover
 */
 

@SpringBootApplication
public class ELKSpringBootApplication{
    public static void main(String[] args) {
		SpringApplication.run(ELKSpringBootApplication.class, args);
	}
}

/*
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class SpringBootApp1 {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootApp1.class, args);
	}
	
}
*/