package com.hcl.training.restEndpoints.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.training.restEndpoints.model.Employee;
import com.hcl.training.restEndpoints.service.EmployeeServiceIntf;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/department")
@Api(value = "Swagger2DemoRestController", description = "REST APIs related to Department Entity!!!!")
public class DepartmentController {
	@Autowired
	EmployeeServiceIntf empService;
	@PostMapping("/saveData")
	public String saveData(@RequestBody Employee employee) {
		empService.saveData(employee);
		
		return "Insertion success";
	}
}