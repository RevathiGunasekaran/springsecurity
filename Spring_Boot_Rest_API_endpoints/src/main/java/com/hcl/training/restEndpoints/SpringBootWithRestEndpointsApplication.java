package com.hcl.training.restEndpoints;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWithRestEndpointsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWithRestEndpointsApplication.class, args);
	}

}
