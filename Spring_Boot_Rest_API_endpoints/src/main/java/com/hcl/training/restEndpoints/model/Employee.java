package com.hcl.training.restEndpoints.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
@Entity
@Table(name="employeedetails") 
public class Employee {
	@Id
	@GeneratedValue(generator="increment")
	private int employeeId;
	@NotEmpty
	private String employeeName;
	@NotEmpty
	private String address;
	@NotEmpty
	private String emailId;
	@NotEmpty
	private String contactNumber;
	@NotEmpty
	private String password;

}
