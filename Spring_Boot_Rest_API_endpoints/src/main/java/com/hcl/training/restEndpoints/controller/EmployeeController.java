package com.hcl.training.restEndpoints.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.training.restEndpoints.model.Employee;
import com.hcl.training.restEndpoints.service.EmployeeServiceIntf;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	@Autowired
	EmployeeServiceIntf empService;
	@PostMapping("/saveData")
	public ResponseEntity<String> saveData(@Valid @RequestBody Employee employee,BindingResult result) {
		if(result.hasErrors()) {
			return new ResponseEntity<>("Insertion failed",HttpStatus.NOT_FOUND);
		}
		empService.saveData(employee);
		return new ResponseEntity<>("Insertion success",HttpStatus.OK);
		
		
	}
	@DeleteMapping("/deleteData/{id}")
	public ResponseEntity<String> deleteData(@PathVariable("id") int id) {
		if(empService.deleteData(id)) {
			return new ResponseEntity<>("Deletion success",HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Deletion failed",HttpStatus.NOT_FOUND);
		}
	}
	@GetMapping("/fetchData")
	public ResponseEntity<List<Employee>> fetchData() {
		List<Employee> employee = new ArrayList<Employee>();
		empService.fetchData().forEach(employee::add);
		if(employee.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(employee,HttpStatus.OK);
	}
	@PutMapping("/updateData/{id}/{address}")
	public ResponseEntity<String> updateData(@PathVariable("id") int id,@PathVariable("address") String address) {
		
		if(empService.updateData(id,address)) {
		return new ResponseEntity<>("Update success",HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Update failed",HttpStatus.NOT_FOUND);
		}
	}
	

}
