package com.hcl.training.restEndpoints.service;

import java.util.List;

import com.hcl.training.restEndpoints.model.Employee;

public interface EmployeeServiceIntf {

	void saveData(Employee employee);

	boolean deleteData(int id);

	List<Employee> fetchData();

	boolean updateData(int id, String address);

}
