package com.hcl.training.restEndpoints.service;

import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.hibernate.query.criteria.internal.predicate.IsEmptyPredicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.training.restEndpoints.model.Employee;
import com.hcl.training.restEndpoints.repository.EmployeeRepository;
@Service
@Transactional
public class EmployeeService implements EmployeeServiceIntf{
@Autowired
EmployeeRepository repo;
	@Override
	public void saveData(Employee employee) {
		String encryptedPassword= Base64.getEncoder().encodeToString(employee.getPassword().getBytes());
		employee.setPassword(encryptedPassword);
		repo.save(employee);
	}
	@Override
	public boolean deleteData(int id) {
		// TODO Auto-generated method stub
		Optional<Employee> list =repo.findById(id);
		if(list.isPresent()) {
			repo.deleteById(id);
			return true;
		}
		return false;
	}
	@Override
	public List<Employee> fetchData() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
	@Override
	public boolean updateData(int id,String name) {
		// TODO Auto-generated method stub
		Optional<Employee> list =repo.findById(id);
		if(list.isPresent()) {
			repo.updateData(id,name);
			return true;
		}
		return false;
		
	}

}
