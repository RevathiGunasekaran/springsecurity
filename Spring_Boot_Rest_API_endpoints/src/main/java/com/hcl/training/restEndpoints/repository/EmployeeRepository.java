package com.hcl.training.restEndpoints.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.training.restEndpoints.model.Employee;
@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer>{

	@Modifying
	@Query("update Employee set address=:address where employeeId=:id")
	void updateData(@Param("id") int id,@Param("address") String address);

}
