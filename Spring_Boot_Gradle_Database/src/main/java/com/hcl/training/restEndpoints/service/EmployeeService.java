package com.hcl.training.restEndpoints.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.training.restEndpoints.model.Employee;
import com.hcl.training.restEndpoints.repository.EmployeeRepository;
@Service
@Transactional
public class EmployeeService implements EmployeeServiceIntf{

	@Autowired
	EmployeeRepository repo;
	public void saveData(Employee emp) {
		repo.save(emp);
		
	}

	public void deleteData(int id) {
		
		repo.deleteById(id);
	}

	
	public List<Employee> fetchAllEmployees() {
		return repo.findAll();
		
	}

	
	public List<Employee> fetchByName(String name) {
		
		return repo.findByEmployeeName(name);
	}

	
	public Optional<Employee> findById(int id) {
		
		return repo.findById(id);
	}

}
