package com.hcl.training.restEndpoints.service;

import java.util.List;
import java.util.Optional;

import com.hcl.training.restEndpoints.model.Employee;

public interface EmployeeServiceIntf {

	void saveData(Employee emp);

	void deleteData(int id);

	List<Employee> fetchAllEmployees();

	List<Employee> fetchByName(String name);

	Optional<Employee> findById(int id);

	

	
}
