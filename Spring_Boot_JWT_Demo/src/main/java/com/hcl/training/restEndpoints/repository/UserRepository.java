package com.hcl.training.restEndpoints.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.training.restEndpoints.model.User;
@Repository
public interface UserRepository extends JpaRepository<User,Integer>{

	User findByUserName(String username);


	

}
