package com.hcl.training.restEndpoints.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.training.restEndpoints.model.AuthRequest;
import com.hcl.training.restEndpoints.util.JwtUtil;

@RestController
public class UserController {
	@Autowired
	JwtUtil util;

	@Autowired
	AuthenticationManager authenticationManger;

	@GetMapping(value = "/")
	public String welcomePage() {

		return "welcome";
	}

	@PostMapping(value = "/authenticate")
	public String authenticate(@RequestBody AuthRequest request) throws Exception {
		try {
			authenticationManger.authenticate(
					new UsernamePasswordAuthenticationToken(request.getUserName(), request.getPassword()));
		} catch (Exception e) {
			throw new Exception("invalid username and password");
		}
		return util.generateToken(request.getUserName());
	}

}
