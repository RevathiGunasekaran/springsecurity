package com.hcl.training.restEndpoints;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.training.restEndpoints.model.User;
import com.hcl.training.restEndpoints.repository.UserRepository;
//https://bezkoder.com/spring-boot-jwt-mysql-spring-security-architecture/
//https://stackoverflow.com/questions/59659822/setting-authentication-in-securitycontext-when-using-jwt
@SpringBootApplication
public class SpringBootWithRestEndpointsApplication {
	@Autowired
	UserRepository repo;

	@PostConstruct
	public void initUsers()
	{
		List<User> users=Stream.of(
				new User(101,"revathi","revathi"),
				new User(102,"drish","drish"),
				new User(102,"vino","vino"))
				.collect(Collectors.toList());
		repo.saveAll(users);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWithRestEndpointsApplication.class, args);
	}

}
