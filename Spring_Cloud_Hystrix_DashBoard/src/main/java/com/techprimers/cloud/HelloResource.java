package com.techprimers.cloud;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/rest/hello/client")
public class HelloResource {


    @Autowired
    private RestTemplate restTemplate;


   
    @HystrixCommand(fallbackMethod = "fallback")
    @GetMapping("/welcome")
    public String welcome() {
        String url = "http://hello-server/rest/hello/server/welcome";
        return restTemplate.getForObject(url, String.class);
    }

    public String fallback(Throwable hystrixCommand) {
        return "Fall Back Hello world";
    }

}
