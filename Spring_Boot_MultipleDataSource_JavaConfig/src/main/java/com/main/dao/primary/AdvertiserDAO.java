package com.main.dao.primary;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.main.model.primary.Advertiser;

@Repository
public class AdvertiserDAO {
 

   @Autowired
   @PersistenceContext(unitName="PERSITENCE_UNIT_NAME_2")
   private EntityManager entityManager;

   public List<Advertiser> listAdvertisers() {
	   return entityManager.createQuery("from Advertiser").getResultList();
   }

   public Advertiser findById(Long id) {
       return this.entityManager.find(Advertiser.class, id);
   }
    
}
