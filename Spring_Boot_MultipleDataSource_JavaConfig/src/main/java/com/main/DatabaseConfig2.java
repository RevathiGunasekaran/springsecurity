package com.main;


import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration

public class DatabaseConfig2 {

   @Autowired
   private Environment env; // Contains Properties Load by @PropertySources

   @Bean
@Primary
   public DataSource ds2Datasource() {
       DriverManagerDataSource dataSource = new DriverManagerDataSource();

       dataSource.setDriverClassName("org.h2.Driver");
       dataSource.setUrl("jdbc:h2:file:~/secondarydb");
       dataSource.setUsername("sa");
       dataSource.setPassword("");

       return dataSource;
   }

   @Bean
   public LocalContainerEntityManagerFactoryBean ds2EntityManager() {
       LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
       em.setDataSource(ds2Datasource());

       // Scan Entities in Package:
       em.setPackagesToScan(new String[] { "com.main.model.secondary" });

       em.setPersistenceUnitName("PERSITENCE_UNIT_NAME_2");
       HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
       em.setJpaVendorAdapter(vendorAdapter);

       HashMap<String, Object> properties = new HashMap<>();
       // JPA & Hibernate
       properties.put("hibernate.dialect","org.hibernate.dialect.H2Dialect");
       properties.put("hibernate.show-sql","true");
       properties.put("hibernate.hbm2ddl.auto", "update");
       // Solved Error: PostGres createClob() is not yet implemented.
       // PostGres Only.
       // properties.put("hibernate.temp.use_jdbc_metadata_defaults",  false);

       em.setJpaPropertyMap(properties);
       em.afterPropertiesSet();
       return em;
   }

   @Bean
   public PlatformTransactionManager ds2TransactionManager() {

       JpaTransactionManager transactionManager = new JpaTransactionManager();
       transactionManager.setEntityManagerFactory(ds2EntityManager().getObject());
       return transactionManager;
   }

}