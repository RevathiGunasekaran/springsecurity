package com.javatechie.spring.auditing.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javatechie.spring.auditing.api.dao.EmployeeRepository;
import com.javatechie.spring.auditing.api.dto.InputRequest;
import com.javatechie.spring.auditing.api.model.Employee;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository repository;

	public String saveEmployee(InputRequest<Employee> request) {
		String currentUser = request.getLoggedInUser();
		Employee employee = request.getEmployee();
		employee.setCreatedBy(currentUser);
		repository.save(employee);
		return "Employee saved successfully...";

	}

	public String updateEmployee(int id, double salary, InputRequest<Employee> request) {
		Optional<Employee> employee = repository.findById(id);
		if (employee.isPresent()) {
			employee.get().setSalary(salary);
			employee.get().setModifiedBy(request.getLoggedInUser());
			repository.save(employee.get());
			return "Employee Updated successfully ";
		} else {
			
			return "Employee not found with id : " + id;
		}
		
	}

}
